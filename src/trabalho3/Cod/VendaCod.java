package trabalho3.Cod;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class VendaCod implements Serializable {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    private Integer id_Venda;

    private Date dataVenda;

    private String pagamento;

    private String vendedor;

    private ClienteCod id_Cliente;

    private TipoFrutaCod tipo;

    private FrutasCod Frutas;

    private List <ItensCod> itensVenda;

    private ItensCod quantidade;

    /**
     * Constructor.
     */
    public VendaCod() {
        this.itensVenda = new ArrayList <ItensCod>();
    }

    public void setId_Venda(Integer id_Venda) {
        this.id_Venda = id_Venda;
    }

    public Integer getId_Venda() {
        return this.id_Venda;
    }

    public void setDataVenda(Date dataVenda) {
        this.dataVenda = dataVenda;
    }

    public Date getDataVenda() {
        return this.dataVenda;
    }

    public void setPagamento(String pagamento) {
        this.pagamento = pagamento;
    }

    public String getPagamento() {
        return this.pagamento;
    }

    public void setVendedor(String vendedor) {
        this.vendedor = vendedor;
    }

    public String getVendedor() {
        return this.vendedor;
    }

    public void setId_Cliente(ClienteCod id_Cliente) {
        this.id_Cliente = id_Cliente;
    }

    public ClienteCod getId_Cliente() {
        return this.id_Cliente;
    }

    public void setItensVendaSet(List<ItensCod> itensVendaSet) {
        this.itensVenda = itensVenda;
    }

    public void addItensVenda(ItensCod itensVenda) {
        this.itensVenda.add(itensVenda);
    }

    public List<ItensCod> getItensVendaSet() {
        return this.itensVenda;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        VendaCod other = (VendaCod) obj;
        if (id_Venda == null) {
            if (other.id_Venda != null) {
                return false;
            }
        } else if (!id_Venda.equals(other.id_Venda)) {
            return false;
        }
        return true;
    }

    public int inserirVenda(VendaCod venda) {

        ConexaoCod c = new ConexaoCod();

        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;
        int id_Fruta = 0;

        String insertTableSQL = "INSERT INTO OO_Venda (id_Venda,datavenda,pagamento,vendedor,id_cliente) VALUES (IDVENDA_SEQ.nextval,?,?,?,?)";

        try {

            String[] generatedColumns = {"id_Venda"};
            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();

            ps.setDate(1, (java.sql.Date) dataVenda);
            ps.setString(2, pagamento);
            ps.setString(3, vendedor);
            ps.setInt(4, id_Cliente.getId_Cliente());

            //execute insert SQL statement
            ps.executeUpdate();

            //ver qual codigo da tua venda
            System.out.println("Record is inserted into OO_Venda table!");

            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
                System.out.println("id gerado: " + chaveGerada);
            }
            venda.setId_Venda(chaveGerada);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id_Venda;
    }

    public TipoFrutaCod getTipo() {
        return tipo;
    }

    public void setTipo(TipoFrutaCod tipo) {
        this.tipo = tipo;
    }

    public FrutasCod getFrutas() {
        return Frutas;
    }

    public void setFrutas(FrutasCod Frutas) {
        this.Frutas = Frutas;
    }

    public ItensCod getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(ItensCod quantidade) {
        this.quantidade = quantidade;
    }

    public static ArrayList<VendaCod> getAll() {
        ConexaoCod c = new ConexaoCod();
        ArrayList<VendaCod> al = new ArrayList<VendaCod>();
        Connection con = c.getConexao();
        PreparedStatement ps = null;
        String sql = "select *from OO_Venda";  // pega tudo oq tem dentro
        try {
            ps = con.prepareStatement(sql);
            ResultSet r = ps.executeQuery(); // executa a string no statement
            while (r.next()) { // passa pelo arraylist
                VendaCod l = new VendaCod(); // se existe uma resposta, cria uma nova Venda
                l.setId_Venda(r.getInt("id_Venda"));
                l.setDataVenda(r.getDate("dataVenda"));
                l.setPagamento(r.getString("pagamento"));
                l.setVendedor(r.getString("vendedor"));
                ClienteCod cli = new ClienteCod();
                //cli.load(); para pegar tudo dentro da classe cliente  
                cli = cli.getById(r.getInt("id_Cliente"));
                if (cli == null) {
                    cli = new ClienteCod();
                    cli.setNome("Não Encontrado");
                }
                l.setId_Cliente(cli);
                al.add(l);
            }
            } catch (SQLException ex) {
            return null;
        }
        return al;
    }

}

    

