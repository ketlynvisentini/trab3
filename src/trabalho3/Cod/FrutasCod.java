package trabalho3.Cod;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class FrutasCod implements Serializable {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    private Integer id_Fruta;

    private TipoFrutaCod tipo;

    private Float valor;

    private int estoque;

    private String sabor;

    private List <ItensCod> itensCod;

    /**
     * Constructor.
     */
    public FrutasCod() {
        this.itensCod = new ArrayList <ItensCod>();
    }

    public void setId_Fruta(Integer id_Fruta) {
        this.id_Fruta = id_Fruta;
    }

    public Integer getId_Fruta() {
        return this.id_Fruta;
    }

    public void setTipo(TipoFrutaCod tipo) {
        this.tipo = tipo;
    }

    public TipoFrutaCod getTipo() {
        return this.tipo;
    }

    public void setValor(Float valor) {
        this.valor = valor;
    }

    public Float getValor() {
        return this.valor;
    }

    public void setItensCod(List <ItensCod> itensCod) {
        this.itensCod = itensCod;
    }

    public void addItensCod(ItensCod itensCod) {
        this.itensCod.add(itensCod);
    }

    public List<ItensCod> getItensCod() {
        return this.itensCod;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FrutasCod other = (FrutasCod) obj;
        if (id_Fruta== null) {
            if (other.id_Fruta != null) {
                return false;
            }
        } else if (!id_Fruta.equals(other.id_Fruta)) {
            return false;
        }
        return true;
    }

    public int getEstoque() {
        return estoque;
    }

    public void setEstoque(int estoque) {
        this.estoque = estoque;
    }

    public int inserirFrutas(FrutasCod Fruta) {

        ConexaoCod c = new ConexaoCod();

        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;
        int id_Fruta = 0;

        String insertTableSQL = "INSERT INTO OO_Frutas (id_Fruta,id_tipo,valor,estoque,sabor) VALUES (IDFruta_SEQ.nextVal,?,?,?,?)";

        try {

            String generatedColumns[] = {"id_Fruta"};
            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();

            ps.setInt(1, tipo.getId_Tipo());
            ps.setFloat(2, valor);
            ps.setInt(3, estoque);
            ps.setString(4, sabor);
            //execute insert SQL statement
            ps.executeUpdate();

            //ver qual codigo da tua venda
            System.out.println("Record is inserted into OO_Frutas table!");

            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
                System.out.println("id gerado: " + chaveGerada);
            }
            Fruta.setId_Fruta(chaveGerada);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id_Fruta;
    }

    public String getSabor() {
        return sabor;
    }

    public void setSabor(String sabor) {
        this.sabor = sabor;
    }

    static public ArrayList<FrutasCod> getAll() { // Retorna um array contendo todos os sushis no banco de dados
        ConexaoCod c = new ConexaoCod();
        ArrayList<FrutasCod> al = new ArrayList<FrutasCod>();
        Connection con = c.getConexao();
        PreparedStatement ps = null, ps2 = null;
        String sql = "select * from OO_Frutas";  // pega tudo oq tem dentro
        try {
            ps = con.prepareStatement(sql);
            ResultSet r = ps.executeQuery(); // executa a string no statement
            while (r.next()) { // passa pelo arraylist
                FrutasCod l = new FrutasCod(); // se existe uma resposta, cria um novo Cliente
                int id_tipo = r.getInt("id_Tipo");

                String sqlTipo = "select * from oo_tipo where id_tipo = " + id_tipo;

                ps2 = con.prepareStatement(sqlTipo);
                ResultSet r2 = ps2.executeQuery(); // e
                
                if(r2.next()){
                    TipoFrutaCod tTemp = new TipoFrutaCod(r2.getString("nome"));
                    tTemp.setId_Tipo(id_tipo);
                    l.setTipo(tTemp);
                }
                
                
                l.setId_Fruta(r.getInt("id_Fruta"));
                l.setValor(r.getFloat("valor"));
                l.setEstoque(r.getInt("estoque"));
                l.setSabor(r.getString("sabor"));

                al.add(l);
            }
        } catch (SQLException ex) {
            return null;
        }
        return al;
    }

    public void delete() { // deleta pelo nome
        ConexaoCod c = new ConexaoCod();
        Connection con = c.getConexao();
        PreparedStatement ps = null;
        String insertTableSQL = "DELETE OO_Frutas where sabor=?";
        try {
            ps = con.prepareStatement(insertTableSQL);
            ps.setString(1, sabor);
            ps.executeUpdate();
            System.out.println("Foi excluido da tabela Frutas!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void update() { // muda uma linha
        ConexaoCod c = new ConexaoCod();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String insertTableSQL = "UPDATE OO_Frutas SET id_tipo=?, valor=?, estoque=? where sabor=?";
        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1, tipo.getId_Tipo());
            preparedStatement.setFloat(2, valor);
            preparedStatement.setInt(3, estoque);
            preparedStatement.setString(4, sabor);
            preparedStatement.executeUpdate();
            System.out.println("Foi atualizado na tabela Frutas!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return  sabor ;
    }

}

